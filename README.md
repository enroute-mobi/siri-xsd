## Usage

To validate a SIRI file:

```
siri-xsd-validate siri.xml
```

In development environement:

```
bundle exec exe/siri-xsd-validate siri.xml
```

To validate files into a directory:

```
siri-xsd-validate --listen path/to/dir
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

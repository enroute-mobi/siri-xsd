require 'nokogiri'

module SIRI
  module XSD
    class Validator

      def self.schema_dir
        File.expand_path("../../../../vendor/xsd",__FILE__)
      end

      def self.schema_file
        File.join(schema_dir, 'soap.xsd')
      end

      def self.load_schema
        Dir.chdir schema_dir do
          File.open schema_file, "r" do |file|
            ::Nokogiri::XML::Schema file
          end
        end
      end

      def self.schema
        @schema ||= load_schema
      end

      def validate_file(file)
        validate file, filename: file
      end

      def validate(input, filename: nil)
        print "Inspect #{filename}" if filename
        open_input(input) do |io|
          document = ::Nokogiri::XML::Document.parse io, filename
          document_errors = self.class.schema.validate(document)

          puts ": #{document_errors.count} error(s)" if filename
          errors.concat document_errors
        end
      end

      def open_input(input)
        if input.respond_to?(:read)
          yield input
        else
          File.open(input,'r') do |file|
            yield file
          end
        end
      end

      def valid?
        errors.empty?
      end

      def errors
        @errrors ||= []
      end

    end
  end
end

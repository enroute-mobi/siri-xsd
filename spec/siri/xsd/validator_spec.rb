require "spec_helper"

RSpec.describe SIRI::XSD::Validator do
  describe "#schema_file" do
    it "returns the vendor/xsd NeTEx_publication.xsd file" do
      expect(File.exist?(SIRI::XSD::Validator.schema_file)).to be_truthy
    end
  end

  it "validates a simple XML file" do
    subject.validate_file "spec/fixtures/raw/siri_exm_SM/exs_stopMonitoring_request.xml"
    expect(subject.errors).to be_empty
  end
end
